

HEADER = [
    "ply",
    "format ascii 1.0",
    "comment This file represents a triangle (© A. Duncan 2022)",
    "element vertex 3",
    "property float32 x",
    "property float32 y",
    "property float32 z",
    "property float32 nx",
    "property float32 ny",
    "property float32 nz",
    "element face 1",
    "property list uint8 int32 vertex_indices",
    "end_header"
]