from const import HEADER

class Mesh(object):
    def __init__(self,filename):
        self.filename = filename
        self.verticals = []
        self.faces = []
        self.texturkoordinaten = []
        self.vertexnormale = []
        try:
            f = open(filename)
            for line in f:
                lineConverted = line.replace("\n","").split(" ")
                if lineConverted[0] == "v":
                    lineSplit = line.replace("\n","").split(" ")
                    lineClean = []
                    for ele in lineSplit:
                        if len(ele.replace(" ", "")) != 0:
                            lineClean.append(ele)
                    vertex = (float(lineClean[1]), float(lineClean[2]), float(lineClean[3]))
                    self.verticals.append(vertex)

                if lineConverted[0] == "vn":
                    lineSplit = line.replace("\n", "").split(" ")
                    lineClean = []
                    for ele in lineSplit:
                        if len(ele.replace(" ", "")) != 0:
                            lineClean.append(ele)
                    vertexnorm = (float(lineClean[1]), float(lineClean[2]), float(lineClean[3]))
                    self.vertexnormale.append(vertexnorm)

                if lineConverted[0] == "vt":
                    lineSplit = line.replace("\n", "").split(" ")
                    lineClean = []
                    for ele in lineSplit:
                        if len(ele.replace(" ", "")) != 0:
                            lineClean.append(ele)
                    texture = (float(lineClean[1]), float(lineClean[2]), float(lineClean[3]))
                    self.texturkoordinaten.append(texture)

                if lineConverted[0] == "f":
                    del lineConverted[0]
                    face = []
                    # set -1 as Flag for no Objekt
                    for element in lineConverted:
                        if element.__contains__("/"):
                            x = element.split('/')
                            if len(x[1]) < 1:
                                facetuples = (int(x[0]) - 1, -1, int(x[2]) - 1)
                            else:
                                facetuples = (int(x[0]) - 1, int(x[1]) - 1, int(x[2]) - 1)
                            face.append(facetuples)
                        else:
                            facetuples = (int(element) - 1, -1, -1)
                            face.append(facetuples)
                        face.sort(key=lambda tup: tup[0])
                    self.faces.append(face)

            f.close()
        except IOError:
            print("Didnt find Obj.")

    def toString(self):
        for v in self.verticals:
            print("verticals", v)
        for f in self.faces:
            print("faces", f)
        for e in self.texturkoordinaten:
            print("texturkoordinaten", e)
        for d in self.vertexnormale:
            print("vertexnormale", d)

    def txtkordPresent(self):
        if len(self.texturkoordinaten) > 0:
            return True
        else:
            return False

    def normPresent(self):
        if len(self.vertexnormale) > 0:
            return True
        else:
            return False

    def countCornesOfObject(self):
        count = 0
        for x in self.faces:
            if len(x) > count:
                count = len(x)
        return count

    def countFaces(self):
        return len(self.faces)

    def countEdges(self):
        # not efficient, pls write again
        count = 0
        for i in range(0, len(self.faces)):
            p1 = [self.faces[i][0][0], self.faces[i][1][0], self.faces[i][2][0]]
            for j in range(i + 1, len(self.faces)):
                p2 = [self.faces[j][0][0], self.faces[j][1][0], self.faces[j][2][0]]
                counter = 0
                for s1 in p1:
                    for s2 in p2:
                        if s1 == s2:
                            counter = counter + 1
                if counter > 1:
                    count = count + 1
        return count

    def createPly(self):
        name = self.filename.split("/")[-1].split(".")[0]
        filename = "%s.ply" % name
        print(filename)
        with open(filename, 'w') as f:
            for line in HEADER:
                f.write(line)
                f.write('\n')




